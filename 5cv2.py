import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import tts
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from mlxtend.evaluate import paired_ttest_5x2cv

data = pd.read_csv('out.csv')
X = data.drop('diagnosis', axis = 1)
y = data.diagnosis

clf1 = LogisticRegression(random_state=1)
clf2 = DecisionTreeClassifier(random_state=1)
clf3 = RandomForestClassifier(n_estimators = 100, max_depth = 4)

X_train, X_test, y_train, y_test = tts(X, y, test_size=0.25, random_state=123)

score1 = clf1.fit(X_train, y_train).score(X_test, y_test)
score2 = clf2.fit(X_train, y_train).score(X_test, y_test)
score3 = clf3.fit(X_train, y_train).score(X_test, y_test)

print('Logistic regression accuracy: %.2f%%' % (score1*100))
print('Decision tree accuracy: %.2f%%' % (score2*100))
print('Random forest classifier: %.2f%%' % (score3*100))

t, p = paired_ttest_5x2cv(estimator1=clf3, estimator2=clf2, X=X, y=y, random_seed=1)

print('t statistic: %.3f' % t)
print('p value: %.3f' % p)