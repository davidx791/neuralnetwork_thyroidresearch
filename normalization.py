import pandas as pd 
import numpy as np 
from sklearn.preprocessing import StandardScaler

def scale(data):
    scaler = StandardScaler()
    scaler.fit(data)
    return scaler.transform(data)

def normalize(data):
    results = []
    for c in data.columns:
        ctype = data[c].dtype
        
        if ctype == 'float64':
            out[c] = scale(data[c].values.reshape(-1,1))
            out[c] = out[c].round(5)
        else:
            out[c] = data[c]

#normalize data and write into a new file
data = pd.read_csv('out.csv')
out = pd.DataFrame()
normalize(data)
out['diagnosis'] = data.diagnosis
out2 = out.drop('Unnamed: 0', axis = 1)
print(out2)
out2.to_csv('normalized.csv',index=False)








