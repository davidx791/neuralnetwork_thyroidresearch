import pandas
import numpy
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.linear_model import LogisticRegression
from sklearn.feature_selection import RFE
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score
from scipy import stats
import statistics as stat
import matplotlib.pyplot as plt

#####################################
data = pandas.read_csv('normalized2.csv')
X = data.drop('diagnosis', axis = 1)
y = data.diagnosis
N = 10
#####################################

def print_ranking_with_labels(ranking,first_n):
    pairs = zip(data.columns, ranking)
    pairs.sort(key = lambda t: t[1])
    pairs = list(reversed(pairs))
    for pair in pairs[:first_n]:
        print ('{:<25} {:>5.2f}'.format(pair[0], pair[1]))

def get_best_features_labels(ranking, best_n):
    pairs = zip(data.columns, ranking)
    pairs.sort(key = lambda t: t[1])
    pairs = list(reversed(pairs))
    names = [i[0] for i in pairs]
    return names[:best_n]

print('\n\nExtraTreesClassifier ranking')
model = ExtraTreesClassifier(n_estimators=100)
model.fit(X, y)
print_ranking_with_labels(model.feature_importances_,N)

max_iter = 400
mlp50 = MLPClassifier(hidden_layer_sizes=(50), max_iter=max_iter,solver='sgd',momentum=1)
mlp100 = MLPClassifier(hidden_layer_sizes=(100), max_iter=max_iter,solver='sgd',momentum=1)
mlp200 = MLPClassifier(hidden_layer_sizes=(200), max_iter=max_iter,solver='sgd',momentum=1)

accuracy50 = []
accuracy100 = []
accuracy200 = []

accuracy_f1_50 = []
accuracy_f1_100 = []
accuracy_f1_200 = []

test_50_100t = []
test_50_200t = []
test_100_200t = []

test_50_100p = []
test_50_200p = []
test_100_200p = []
for i in range(1, len(X.columns) + 1):
    names = get_best_features_labels(model.feature_importances_, i)

    score50 = []
    score100 = []
    score200 = []

    f1_50 = []
    f1_100 = []
    f1_200 = []

    for j in range(5):
        X_train, X_test, y_train, y_test = \
            train_test_split(X[names], y, test_size=0.5, random_state=1)

        # SCORES CV2 mlp50, mlp100, mlp200
        score50.append(mlp50.fit(X_train, y_train).score(X_test, y_test))
        score50.append(mlp50.fit(X_test, y_test).score(X_train, y_train))  # test <--> train

        score100.append(mlp100.fit(X_train, y_train).score(X_test, y_test))
        score100.append(mlp100.fit(X_test, y_test).score(X_train, y_train))  # test <--> train

        score200.append(mlp200.fit(X_train, y_train).score(X_test, y_test))
        score200.append(mlp200.fit(X_test, y_test).score(X_train, y_train))  # test <--> train

        # F1 SCORES mlp50, mlp100, mlp200
        f1_50.append(f1_score(y_test,  mlp50.predict(X_test)))
        f1_50.append(f1_score(y_train, mlp50.predict(X_train))) #test <--> train

        f1_100.append(f1_score(y_test,  mlp100.predict(X_test)))
        f1_100.append(f1_score(y_train, mlp100.predict(X_train))) #test <--> train

        f1_200.append(f1_score(y_test,  mlp200.predict(X_test)))
        f1_200.append(f1_score(y_train, mlp200.predict(X_train))) #test <--> train

    scores = np.array([100 * stat.mean(score50), 100 * stat.mean(score100), 100 * stat.mean(score200)])
    f1=np.array([stat.mean(f1_50),stat.mean(f1_100),stat.mean(f1_200)])

    t50_100, p50_100 = stats.ttest_ind(score100, score50)
    t50_200, p50_200 = stats.ttest_ind(score200, score50)
    t100_200, p100_200 = stats.ttest_ind(score200, score100)

    print('\nNumber of features used: {}'.format(i))
    print('MLP (50 neurons) accuracy:  %.2f%%' % (scores[0]))
    print('MLP (100 neurons) accuracy: %.2f%%' % (scores[1]))
    print('MLP (200 neurons) accuracy: %.2f%%' % (scores[2]))
    print('\nF1 score: [0 ... 1]')
    print('MLP (50 neurons):  %.4f' % f1[0])
    print('MLP (100 neurons): %.4f' % f1[1])
    print('MLP (200 neurons): %.4f' % f1[2])
    print('\nComparison of classifiers')
    print('MLP 100 vs MLP 50:', t50_100, '  ', p50_100)
    print('MLP 200 vs MLP 50:', t50_200, '  ', p50_200)
    print('MLP 200 vs MLP 100:', t100_200, '  ', p100_200)

    accuracy50.append(scores[0])
    accuracy100.append(scores[1])
    accuracy200.append(scores[2])

    accuracy_f1_50.append(f1[0])
    accuracy_f1_100.append(f1[1])
    accuracy_f1_200.append(f1[2])

    test_50_100t.append(t50_100)
    test_50_200t.append(t50_200)
    test_100_200t.append(t100_200)

    test_50_100p.append(p50_100)
    test_50_200p.append(p50_200)
    test_100_200p.append(p100_200)

    print('\n\n')

print('\nEND OF CALCULATIONS')

print('PLOTING ...')
epochs = range(1, len(X.columns))
plt.plot(epochs, accuracy50, label='50 neurons')
plt.plot(epochs, accuracy100, label='100 neurons')
plt.plot(epochs, accuracy200, label='200 neurons')
plt.xlabel('Number of features')
plt.ylabel('Accuracy')
plt.legend()
plt.show()

print('SAVING ...')
with open('final_sgd_mom1.txt', 'w') as f:
    for i in accuracy50, accuracy100, accuracy200, \
             accuracy_f1_50, accuracy_f1_100, accuracy_f1_200, \
             test_50_100t, test_50_200t, test_100_200t, \
             test_50_100p, test_50_200p, test_100_200p:
        f.write("%s\n" % i)

# print('LogisticRegression')
# model_lr = LogisticRegression(solver='liblinear')
# rfe = RFE(model_lr, 3)
# fit = rfe.fit(X, y)
# print_ranking_with_labels(fit.ranking_)

# print('\n\nSelectKBest')
# test = SelectKBest(score_func=chi2, k=4)
# fit = test.fit(X, y)
# features = fit.transform(X)
# print_ranking_with_labels(fit.scores_.astype(int))