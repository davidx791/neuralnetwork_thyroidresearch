import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt

accuracy50 = []
accuracy100 = []
accuracy200 = []

accuracy_f1_50 = []
accuracy_f1_100 = []
accuracy_f1_200 = []

test_50_100t = []
test_50_200t = []
test_100_200t = []

test_50_100p = []
test_50_200p = []
test_100_200p = []

stringList = []
f = open('solver_sgd_momentum09.txt', 'r')

i = 0
for line in f:
    i += 1
    stringList = line.replace("[","").replace("]","").replace(",","")
    if i == 1:
        accuracy50 = [float(x) for x in stringList.split()]
    if i == 2:
        accuracy100 = [float(x) for x in stringList.split()]
    if i == 3:
        accuracy200 = [float(x) for x in stringList.split()]
    if i == 4:
       accuracy_f1_50 = [float(x) for x in stringList.split()]
    if i == 5:
        accuracy_f1_100 = [float(x) for x in stringList.split()]
    if i == 6:
        accuracy_f1_200 = [float(x) for x in stringList.split()]
    if i == 7:
        test_50_100t = [float(x) for x in stringList.split()]
    if i == 8:
        test_50_200t = [float(x) for x in stringList.split()]
    if i == 9:
        test_100_200t = [float(x) for x in stringList.split()]
    if i == 10:
        test_50_100p = [float(x) for x in stringList.split()]
    if i == 11:
        test_50_200p = [float(x) for x in stringList.split()]
    if i == 12:
        test_100_200p = [float(x) for x in stringList.split()]

#print('accuracy50: ',accuracy50)
#print('accuracy50: ',accuracy100)
#print('accuracy50: ',accuracy200)

#print('accuracy_f1_50: ',accuracy_f1_50)
#print('accuracy_f1_100: ',accuracy_f1_100)
#print('accuracy_f1_200: ',accuracy_f1_200)

#print('test_50_100t: ',test_50_100t)
#print('test_50_200t: ',test_50_200t)
#print('test_100_200t: ', test_100_200t)

#print('test_50_100p: ', test_50_100p)
#print('test_50_200p: ', test_50_200p)
#print('test_100_200p: ', test_100_200p)


#MEDIAN FILTR
from scipy.signal import medfilt
accuracy50med = medfilt(accuracy50,5)
accuracy100med = medfilt(accuracy100,5)
accuracy200med = medfilt(accuracy200,5)

accuracy_f1_50med = medfilt(accuracy_f1_50,5)
accuracy_f1_100med = medfilt(accuracy_f1_100,5)
accuracy_f1_200med = medfilt(accuracy_f1_200,5)

test_50_100tmed = medfilt(test_50_100t,5)
test_50_200tmed = medfilt(test_50_200t,5)
test_100_200tmed = medfilt(test_100_200t,5)

test_50_100pmed = medfilt(test_50_100p,5)
test_50_200pmed = medfilt(test_50_200p,5)
test_100_200pmed = medfilt(test_100_200p,5)


#PLOT
import matplotlib.pyplot as plt
epochs = range(1, 30)

plt.figure(1)
plt.plot(epochs, accuracy50,linewidth=2, label='50 neurons')
plt.plot(epochs, accuracy100, linewidth=2,label='100 neurons')
plt.plot(epochs, accuracy200,linewidth=2, label='200 neurons')
plt.xlabel('Number of features')
plt.ylabel('Accuracy [%]')
plt.title('Classifiers accuracy')
plt.axis([0, 30, 50, 100])
plt.grid()
plt.legend()

plt.figure(2)
plt.plot(epochs, accuracy_f1_50,linewidth=2, label='50 neurons')
plt.plot(epochs, accuracy_f1_100,linewidth=2, label='100 neurons')
plt.plot(epochs, accuracy_f1_200, linewidth=2,label='200 neurons')
plt.xlabel('Number of features')
plt.ylabel('F1 Scores')
plt.title('F1 Scores')
plt.axis([0, 30, 0, 1])
plt.grid()
plt.legend()

plt.figure(3)
plt.plot(epochs, accuracy50med, linewidth=2,label='50 neurons')
plt.plot(epochs, accuracy100med,linewidth=2, label='100 neurons')
plt.plot(epochs, accuracy200med,linewidth=2, label='200 neurons')
plt.xlabel('Number of features')
plt.ylabel('Accuracy (median)')
plt.title('Classifiers accuracy')
plt.axis([0, 30, 50, 100])
plt.grid()
plt.legend()

plt.figure(4)
plt.plot(epochs, accuracy_f1_50med,linewidth=2, label='50 neurons')
plt.plot(epochs, accuracy_f1_100med,linewidth=2, label='100 neurons')
plt.plot(epochs, accuracy_f1_200med, linewidth=2,label='200 neurons')
plt.xlabel('Number of features')
plt.ylabel('F1 Scores (median)')
plt.title('F1 Scores')
plt.axis([0, 30, 0, 1])
plt.grid()
plt.legend()

plt.figure(5)
plt.plot(epochs, test_50_100tmed,linewidth=2, label='mlp100 vs mlp50')
plt.plot(epochs, test_50_200tmed,linewidth=2, label='mlp200 vs mlp50')
plt.plot(epochs, test_100_200tmed, linewidth=2,label='mlp200 vs mlp100')
plt.xlabel('Number of features')
plt.ylabel('t-statistic (median)')
plt.title('Classifiers comparison: t-statistic')
plt.axis([0, 30, -1, 3])
plt.grid()
plt.legend()

plt.figure(6)
plt.plot(epochs, test_50_100pmed,linewidth=2, label='mlp100 vs mlp50')
plt.plot(epochs, test_50_200pmed, linewidth=2,label='mlp200 vs mlp50')
plt.plot(epochs, test_100_200pmed, linewidth=2,label='mlp200 vs mlp100')
plt.xlabel('Number of features')
plt.ylabel('p-value (median)')
plt.title('Classfiers comparison: p-value')
plt.axis([0, 30, 0, 1])
plt.grid()
plt.legend()


clfList = []
clfList.append(accuracy50med)
clfList.append(accuracy100med)
clfList.append(accuracy200med)
clfNames=['mlp50','mlp100','mlp200']
fig = plt.figure(7)
#plt.title('Classifiers average accuracy (median)')
fig_ax = fig.add_subplot(111)
plt.boxplot(clfList)
plt.title('Classifiers average accuracy (median)')
fig_ax.set_xticklabels(clfNames)

plt.show()



       




