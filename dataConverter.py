import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score
from scipy import stats

#Reading from file
X = pd.read_csv('thyroid.csv')
print(X)
print('\n\n')

#Data conversion
scores = []
for j in range(len(X.columns)):
  print( j,' ',X.columns[j])
  col = X.columns[j]
  
  if j==0 or j==17 or j==19 or j==21 or j==23 or j==25 or j==27:
    result=0
    counter=0
    for i in range(len(X[col].values)):
      if X[col].values[i]!= "?":
        result+=float(X[col].values[i])
        counter+=1
    for i in range(len(X[col].values)):
      if X[col].values[i]== "?":
        X[col].values[i]=float(result/counter)

  if j==0: #wrong age
    for i in range(len(X[col].values)):
      if X[col].values[i] > 100:
        X[col].values[i]=100
               
  if j==28: #refferal source
    ref_source = ["WEST","STMW","SVHC","SVI","SVHD","other"]
    for k in range (0,6):
      for i in range(len(X[col].values)):
        if X[col].values[i] == ref_source[k]:
          X[col].values[i]=k
        
  if j==29: #diagnosis
    for i in range(len(X[col].values)):
      if X[col].values[i] == '-':
        X[col].values[i]=0
      else:
        X[col].values[i]=1
  
#Creating new file with converted data
new_data=X
new_data.to_csv('thyroid_converted.csv', sep=',')

