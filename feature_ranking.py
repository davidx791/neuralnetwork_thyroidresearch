import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score
from scipy import stats

data = pd.read_csv('thyroid_nulls_bindiag.csv')
X = data.drop('diagnosis', axis = 1)

continuous_features = [f for f in X.columns if data[f].dtype == 'object']
binary_features = [f for f in X.columns if data[f].dtype == 'int64']


def fill_binary_nulls_with_proper_distribution(column_name):
    col = X[column_name]
    notnull = list(filter(lambda a: a != '?', col))
    nulls_count = len(col)-len(notnull)
    nulls_ratio = float(nulls_count) / float(len(col))
    return [str(np.random.choice([0,1], p=[nulls_ratio, 1-nulls_ratio])) if x=='?' else x for x in col]
    #print len(X[c]), len(notnull), nulls_count, nulls_ratio, rand

def perform_ranking(data):
    results = []
    for c in data.columns:
        notnull = list(filter(lambda a: a != '?', data[c]))
        ctype = data[c].dtype
        
        if ctype == 'int64' or ctype == 'float64':
            testout = stats.kstest(notnull, 'norm')
            #out[c] = data[c]
            results.append((data[c].name, testout.statistic, testout.pvalue))

        elif ctype == 'object':
            notnull_f = map(float, notnull)
            mean = sum(notnull_f) / len(notnull_f)
            averaged = map(lambda x: str.replace(x, '?', str(mean)), data[c]) # replace nulls with mean
            averaged = map(float, averaged)
            #out[c] = averaged
            testout = stats.kstest(averaged, 'norm')
            results.append((data[c].name, testout.statistic, testout.pvalue))
        else:
            raise(BaseException("Unknown Type"))

    results.sort(key=lambda tup: tup[1],reverse=True)
    print('{:<25} {:>15} {:>15}'.format("NAME", "STAT", "PVALUE"))
    for elem in results:
        print('{:<25} {:>15} {:>15}'.format(elem[0], elem[1], elem[2]))


results = []
out = pd.DataFrame()

X['sex(0=f)'] = map(int, fill_binary_nulls_with_proper_distribution('sex(0=f)'))

for c in X.columns:
    notnull = list(filter(lambda a: a != '?', X[c]))
    
    if X[c].dtype == 'int64' or X[c].dtype == 'float64':
        testout = stats.kstest(notnull, 'norm')
        out[c] = X[c]
        results.append((X[c].name, testout.statistic, testout.pvalue))

    if X[c].dtype == 'object':
        notnull_f = map(float, notnull)
        mean = sum(notnull_f) / len(notnull_f)
        averaged = map(lambda x: str.replace(x, '?', str(mean)), X[c]) # replace nulls with mean
        averaged = map(float, averaged)
        out[c] = averaged
        testout = stats.kstest(averaged, 'norm')
        results.append((X[c].name, testout.statistic, testout.pvalue))
        #print X[c].name, "TOTAL:", len(averaged), "NULLS:", len(X[c])-len(notnull)
        # print('{:<25} {:>15} {:>15}'.format(X[c].name, testout.statistic, testout.pvalue))

results.sort(key=lambda tup: tup[1],reverse=True)
print('{:<25} {:>15} {:>15}'.format("NAME", "STAT", "PVALUE"))
for elem in results:
    print('{:<25} {:>15} {:>15}'.format(elem[0], elem[1], elem[2]))

perform_ranking(out)

out['diagnosis'] = data.diagnosis
out.to_csv('out.csv')

num_features = len(X.columns)
# for i in range(num_features):
#     col = X.columns[i]
#     testout = stats.kstest(col, 'norm')
#     print testout


# X = data.drop('diagnosis', axis = 1)
# y = data.diagnosis

# print X

# clf = RandomForestClassifier(n_estimators = 50, max_depth = 4)

# scores = []
# num_features = len(X.columns)
# for i in range(num_features):
#     col = X.columns[i]
#     score = np.mean(cross_val_score(clf, X[col].values.reshape(-1,1), y, cv=10))
#     scores.append((int(score*100), col))

# print(sorted(scores, reverse = True))


